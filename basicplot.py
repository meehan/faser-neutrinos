from ROOT import TFile,TTree,TH1D,TH2D,TCanvas,gDirectory

# import the necessary local tools
import sys
from MyTools import *
from AtlasStyleTools import *
SetAtlasStyle()

f = TFile("fmillion.root")
t = f.Get("t1")

c1 = TCanvas("c1","c1",500,500)
c2 = TCanvas("c2","c2",580,500)
c2.SetRightMargin(0.20)



types=[]
# types.append({"flavor":"electron" , "id":"12" , "lepid":"11"})
# types.append({"flavor":"muon" , "id":"14" , "lepid":"13"})
types.append({"flavor":"tau" , "id":"16" , "lepid":"15"})
# types.append({"flavor":"antielectron" , "id":"-12" , "lepid":"-11"})
# types.append({"flavor":"antimuon" , "id":"-14" , "lepid":"-13"})
# types.append({"flavor":"antitau" , "id":"-16" , "lepid":"-15"})


outtype="png"


for type in types:



  c1.cd()
  t.Draw("par1_flav >> h1(1000,-500,500)","abs(neutrino_flav)=="+type["id"]+" && par1_has!=0","colz")
  h1 = gDirectory.Get("h1")
  h1.SetStats(0)
  h1.GetXaxis().SetTitle("Parent Flavor")
  h1.GetYaxis().SetTitle("N(neutrinos)")
  h1.SetMaximum(1.5*GetMaxFromHists([h1])[2])
  
  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/parent1_flavor_"+type["flavor"]+"."+outtype)
  
  h1.GetXaxis().SetRangeUser(400,500)
  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/parent1_flavor_"+type["flavor"]+"_zoom1."+outtype)

  h1.GetXaxis().SetRangeUser(-20,20)
  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/parent1_flavor_"+type["flavor"]+"_zoom2."+outtype)


  c1.cd()
  t.Draw("par2_flav >> h1(1000,-500,500)","abs(neutrino_flav)=="+type["id"],"colz")
  h1 = gDirectory.Get("h1")
  h1.SetStats(0)
  h1.GetXaxis().SetTitle("Parents Parent Flavor")
  h1.GetYaxis().SetTitle("N(neutrinos)")
  h1.SetMaximum(1.5*GetMaxFromHists([h1])[2])

  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/parent2_flavor_"+type["flavor"]+"."+outtype)
  
  h1.GetXaxis().SetRangeUser(-20,20)
  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/parent2_flavor_"+type["flavor"]+"_zoom1."+outtype)

  h1.GetXaxis().SetRangeUser(400,500)
  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/parent2_flavor_"+type["flavor"]+"_zoom2."+outtype)
  

  c1.cd()
  t.Draw("neutrino_count_final >> h1(10,0,10)","abs(neutrino_flav)=="+type["id"],"colz")
  h1 = gDirectory.Get("h1")
  h1.SetStats(0)
  h1.GetXaxis().SetTitle("N(neutrinos) per event")
  h1.GetYaxis().SetTitle("NEvents")
  h1.SetMaximum(1.5*GetMaxFromHists([h1])[2])

  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/neutrinocount_"+type["flavor"]+"."+outtype)



  c1.cd()
  t.Draw("neutrino_count_final >> h1(10,0,10)","abs(neutrino_flav)=="+type["id"]+" && par1_flav=="+type["lepid"],"colz")
  h1 = gDirectory.Get("h1")
  h1.SetStats(0)
  h1.GetXaxis().SetTitle("N(neutrinos) per event")
  h1.GetYaxis().SetTitle("NEvents")
  h1.SetMaximum(1.5*GetMaxFromHists([h1])[2])

  h1.Draw()
  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/neutrinocount_"+type["flavor"]+"."+outtype)
  
  

  c1.cd()
  t.Draw("abs(neutrino_eta) >> h1(100,0,10)","abs(neutrino_flav)=="+type["id"]+" && par1_has!=0","colz")
  h1 = gDirectory.Get("h1")
  h1.SetStats(0)
  h1.GetXaxis().SetTitle("abs[eta]")
  h1.GetYaxis().SetTitle("N(neutrinos)")
  h1.SetMaximum(1.5*GetMaxFromHists([h1])[2])
  h1.Draw("colz")

  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SaveAs("plots/neutrino_eta_"+type["flavor"]+"."+outtype)
  
  
  c1.cd()
  t.Draw("abs(neutrino_e) >> h1(100,0,1000)","abs(neutrino_flav)=="+type["id"]+" && par1_has!=0","colz")
  h1 = gDirectory.Get("h1")
  h1.SetStats(0)
  h1.GetXaxis().SetTitle("Energy [GeV]")
  h1.GetYaxis().SetTitle("N(neutrinos)")
  h1.SetMaximum(1.5*GetMaxFromHists([h1])[2])
  h1.Draw("colz")

  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c1.SetLogy()
  c1.SaveAs("plots/neutrino_energy_"+type["flavor"]+"."+outtype)
  
  
  c2.cd()
  t.Draw("abs(neutrino_eta):neutrino_e >> h2(100,0,1000,150,0,15)","abs(neutrino_flav)=="+type["id"]+" && par1_has!=0","colz")
  h2 = gDirectory.Get("h2")
  h2.SetStats(0)
  h2.GetXaxis().SetTitle("Energy [GeV]")
  h2.GetYaxis().SetTitle("abs[eta]")
  h2.GetZaxis().SetTitle("N(neutrinos)")
  h2.Draw("colz")

  ATLASLabel(0.2, 0.9, 0.25, 1, 0.05, "Internal")
  myText(0.2,0.85,1,0.04,"Neutrino Flavor : "+type["flavor"])
  c2.SaveAs("plots/neutrino_eta_energy_"+type["flavor"]+"."+outtype)
  
  c2.SetLogz(1)
  c2.SaveAs("plots/neutrino_eta_energy_"+type["flavor"]+"_logz."+outtype)
  
  c2.SetLogx(1)
  c2.SetLogz(0)
  c2.SaveAs("plots/neutrino_eta_energy_"+type["flavor"]+"_logx."+outtype)
  
  c2.SetLogx(1)
  c2.SetLogz(1)
  c2.SaveAs("plots/neutrino_eta_energy_"+type["flavor"]+"_logx_logz."+outtype)
  
  
  
  
  
  
  
  
  
  
  
  
  
  