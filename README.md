# faser-neutrinos

This is a small pythia package that studies tau neutrino production

# Setup

You will need to install the following two :
   - [ROOT](https://root.cern.ch/) : ROOT with pyROOT enabled
   - [numpythia](https://github.com/scikit-hep/numpythia) : This is just single ``pip install``` call
   
# Production

The first step is to produce events.  This is done with the `run.py` script 
which runs pythia via numpythia.  Everything is hardcoded at this
point, in case you want to change anything, and you can run this via
```
python run.py
```
which will produce an ntuple that saves the relevant information concerning
the neutrinos you produce.

The configuration of the physics is pulled from the `config.cmd` pythia configuration file.


# Plotting

Again, everything is hardcoded.  The analysis/plotting is done via TTree:Draw() and
comes in the `basicplot.py` script.  Run this via
```
python basicplot.py
```
after producing the input root file with TTree