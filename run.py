# https://github.com/scikit-hep/numpythia
from numpythia import Pythia

import ROOT
from ROOT import TH1F,TH2F,TH3F,TFile,TLorentzVector,TTree

from array import array

# CHARMED MESONS
# D+          411
# D0          421
# D∗0(2400)+  10411
# D∗0(2400)0  10421
# D∗(2010)+   413
# D∗(2007)0   423
# D1(2420)+   10413
# D1(2420)0   10423
# D1(H)+      20413
# D1(2430)0   20423
# D∗2(2460)+  415
# D∗2(2460)0  425
# D+s         431
# D∗s0(2317)+ 10431
# D∗+s        433
# Ds1(2536)+  10433
# Ds1(2460)+  20433
# D∗s2(2573)+ 435   

charmMesons = [411,421,10411,10421,413,423,10413,10423,20413,20423,415,425,431,10431,433,10433,20433,435]

dsMesons = [431,10431,433,10433,20433,435]

# LEPTONS 
# e−  11 
# νe  12 
# µ−  13 
# νµ  14 
# τ−  15 
# ντ  16 
# τ− 17 
# ντ 18

# charged leptons
leptons   = [11,13,15,-11,-13,-15]

# neutrinos
neutrinos = [12,14,16,-12,-14,-16]


h_npar  = TH2F("h_npar","",40,-20,20,100,0,100)
h_orig  = TH2F("h_orig","",40,-20,20,1000,-500,500)
h_orig2 = TH2F("h_orig2","",40,-20,20,1000,-500,500)


# ttree initialization
fout = TFile("fout.root","RECREATE")
t = TTree( 't1', 'tree with histos' )


neutrino_count = array( 'i', [ 0 ] )
neutrino_count_final = array( 'i', [ 0 ] )

neutrino_has   = array( 'i', [ 0 ] )
neutrino_flav  = array( 'i', [ 0 ] )
neutrino_e     = array( 'f', [ 0 ] )
neutrino_eta   = array( 'f', [ 0 ] )
neutrino_phi   = array( 'f', [ 0 ] )
 
par1_has       = array( 'i', [ 0 ] )
par1_flav      = array( 'i', [ 0 ] )
par1_e         = array( 'f', [ 0 ] )
par1_eta       = array( 'f', [ 0 ] )
par1_phi       = array( 'f', [ 0 ] )
     
par2_has       = array( 'i', [ 0 ] )
par2_flav      = array( 'i', [ 0 ] )
par2_e         = array( 'f', [ 0 ] )
par2_eta       = array( 'f', [ 0 ] )
par2_phi       = array( 'f', [ 0 ] )


t.Branch( 'neutrino_count',  neutrino_count,  'neutrino_count/I' )
t.Branch( 'neutrino_count_final',  neutrino_count_final,  'neutrino_count_final/I' )

t.Branch( 'neutrino_has',  neutrino_has,  'neutrino_has/I' )
t.Branch( 'neutrino_flav', neutrino_flav, 'neutrino_flav/I' )
t.Branch( 'neutrino_e'  ,  neutrino_e,    'neutrino_e/F' )
t.Branch( 'neutrino_eta',  neutrino_eta,  'neutrino_eta/F' )
t.Branch( 'neutrino_phi',  neutrino_phi,  'neutrino_phi/F' )

t.Branch( 'par1_has',  par1_has,  'par1_has/I' )
t.Branch( 'par1_flav', par1_flav, 'par1_flav/I' )
t.Branch( 'par1_e'  ,  par1_e,    'par1_e/F' )
t.Branch( 'par1_eta',  par1_eta,  'par1_eta/F' )
t.Branch( 'par1_phi',  par1_phi,  'par1_phi/F' )

t.Branch( 'par2_has',  par2_has,  'par2_has/I' )
t.Branch( 'par2_flav', par2_flav, 'par2_flav/I' )
t.Branch( 'par2_e'  ,  par2_e,    'par2_e/F' )
t.Branch( 'par2_eta',  par2_eta,  'par2_eta/F' )
t.Branch( 'par2_phi',  par2_phi,  'par2_phi/F' )


# generate events
pythia = Pythia(config='config.cmd')
events = pythia(events=10000000)
print(type(events))

# loop over events
for e in events:  
  
  neutrino_count[0] = 0
  
  neutrino_count_final[0] = 0
 
  neutrino_has[0]   = -1
  neutrino_flav[0]  = -666
  neutrino_e[0]     = -1
  neutrino_eta[0]   = -1
  neutrino_phi[0]   = -1
  
  par1_has[0]       = -1
  par1_flav[0]      = -666
  par1_e[0]         = -1
  par1_eta[0]       = -1
  par1_phi[0]       = -1
      
  par2_has[0]       = -1
  par2_flav[0]      = -666
  par2_e[0]         = -1
  par2_eta[0]       = -1
  par2_phi[0]       = -1


  # get the event record
  allparts = e.all(return_hepmc=True)
  
  # first go through and count the number of neutrinos in this event
  for p in allparts:
    if p.pid in neutrinos:   
      # just to make it easier to read
      neutrino = p   
      
      # if you find a neutrino, increment this
      neutrino_count[0] += 1
      
      if p.status==1:
         neutrino_count_final[0] += 1

  # now go through and treat each neutrino separately for this event
  # events can be categorized by the NNeutrinos
  for p in allparts:
  
    neutrino_has[0]   = -1
    neutrino_flav[0]  = -1
    neutrino_e[0]     = -1
    neutrino_eta[0]   = -1
    neutrino_phi[0]   = -1

    par1_has[0]       = -1
    par1_flav[0]      = -1
    par1_e[0]         = -1
    par1_eta[0]       = -1
    par1_phi[0]       = -1
    
    par2_has[0]       = -1
    par2_flav[0]      = -1
    par2_e[0]         = -1
    par2_eta[0]       = -1
    par2_phi[0]       = -1
  
    if p.pid in neutrinos:   
      # just to make it easier to read
      neutrino = p   

      # fill the tree of info for this neutrino
      neutrino_has[0]       = 1
      neutrino_flav[0]      = neutrino.pid
      neutrino_e[0]         = neutrino.e
      neutrino_eta[0]       = neutrino.eta
      neutrino_phi[0]       = neutrino.phi

      par1_has[0]       = 1
      par1_flav[0]      = neutrino.parents(return_hepmc=True)[0].pid
      par1_e[0]         = neutrino.parents(return_hepmc=True)[0].e
      par1_eta[0]       = neutrino.parents(return_hepmc=True)[0].eta
      par1_phi[0]       = neutrino.parents(return_hepmc=True)[0].phi
      
      # going back two steps in the parent because of intermediate W
      if neutrino.parents(return_hepmc=True)[0].pid in leptons:
        
        par2_has[0]       = 1
        par2_flav[0]      = neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0].pid
        par2_e[0]         = neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0].e
        par2_eta[0]       = neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0].eta
        par2_phi[0]       = neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0].phi
      
      
      # how many parents does a neutrino have
      h_npar.Fill(neutrino.pid, len(neutrino.parents(return_hepmc=True)))
      
      # what was the flavor of the parent
      if neutrino.parents(return_hepmc=True)[0].pid<-500:
        h_orig.Fill(neutrino.pid, -499)                
      elif neutrino.parents(return_hepmc=True)[0].pid>500:
        h_orig.Fill(neutrino.pid, 499)      
      else:
        h_orig.Fill(neutrino.pid, neutrino.parents(return_hepmc=True)[0].pid)
        
      # going back two steps in the parent because of intermediate W
      if neutrino.parents(return_hepmc=True)[0].pid in leptons:
        if neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0].pid<-500:
          h_orig2.Fill(neutrino.pid, -499)
        elif neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0].pid>500:
          h_orig2.Fill(neutrino.pid, 499)      
        else:
          h_orig2.Fill(neutrino.pid, neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0].pid)
          
      # plot neutrino and parent directions if the parent is the charmMeson
      if neutrino.parents(return_hepmc=True)[0].pid not in leptons:
      
        par1 = neutrino.parents(return_hepmc=True)[0]
        
        p4_neutrino = TLorentzVector()
        p4_neutrino.SetPxPyPzE(neutrino.px,neutrino.py,neutrino.pz,neutrino.e)
        
        p4_parent1 = TLorentzVector()
        p4_parent1.SetPxPyPzE(par1.px,par1.py,par1.pz,par1.e)
        

      
      # but if the parent is itself a lepton, then we need to get the spread 
      # of that intermediate particle and the hadron from whence it CAME!
      if neutrino.parents(return_hepmc=True)[0].pid in leptons:
      
        par1 = neutrino.parents(return_hepmc=True)[0]
        par2 = neutrino.parents(return_hepmc=True)[0].parents(return_hepmc=True)[0]
      
        p4_neutrino = TLorentzVector()
        p4_neutrino.SetPxPyPzE(neutrino.px,neutrino.py,neutrino.pz,neutrino.e)
        
        p4_parent1 = TLorentzVector()
        p4_parent1.SetPxPyPzE(par1.px,par1.py,par1.pz,par1.e)
        
        p4_parent2 = TLorentzVector()
        p4_parent2.SetPxPyPzE(par2.px,par2.py,par2.pz,par2.e)
        
        
        
      # now fill into tree
      t.Fill()
        
      

fout.cd()
t.Write()
h_npar.Write()
h_orig.Write()
h_orig2.Write()
fout.Close()      






