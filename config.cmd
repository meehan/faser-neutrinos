!beam parameters
Beams:idA = 2212 ! first incoming beam is a 2212, i.e. a proton.
Beams:idB = 2212 ! second beam is also a proton.
Beams:eCM = 13000. ! the cm energy of collisions.

!processes
HardQCD:gg2ccbar = on


! turn off underlying event
PartonLevel:ISR = off
PartonLevel:FSR = on
PartonLevel:MPI = off